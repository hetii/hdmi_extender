#!/usr/bin/env python
import os
import sys
import time
import subprocess


def eeprom_dump(bus, address):
    command = ['i2cdump', '-y', str(bus), str(address)]
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None)

    for line in process.communicate()[0].split('\n')[1:]:
        if line:
            values = line[4:51].split(' ')
            print values

def eeprom_write(bus, chip_address, data_address, value):
    command = ['i2cset', '-y', str(bus), str(chip_address), str(data_address), str(value)]
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None)
    res = process.communicate()
    if res[0]: print res[0]
    time.sleep(0.01)

def eeprom_erase(bus, address):
    for x in xrange(256):
        print "Erase data address: 0x%x" % x
        eeprom_write(bus, address, hex(x), 0xff)

def eeprom_erase_all_pages():
    #eeprom_dump(2, 0x50)
    for address in range(0x50, 0x57+1):
        print(hex(address))
        eeprom_erase(7, address)


def parse_edid(file_path, bus, chip_address):
    if os.path.exists(file_path):
        print "file to load:", file_path
        with open(file_path, "rb") as f:
             byte = 1 
             i = 0
             while byte:
                 # Do stuff with byte.
                byte = f.read(1)
                if byte:
                    value = "0x%s" % byte.encode('hex')
                    data_address = "0x%x" % i
                    print "Writing byte %s to address %s" % (value, data_address)
                    i+=1
                    eeprom_write(bus, chip_address, data_address, value)
                else:
                    print "skipp"

#eeprom_erase(7, 0x50)
#parse_edid(sys.argv[1], 7, 0x50)
